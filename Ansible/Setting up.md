# Setting up ansible

Hello everyone!

If you are here it means you are interested in setting up Ansible on your machine to configure your remote hosts!

## Installing ansible

The first step will be, as usual, to install the tool we need! Ansible is great because you only need to install it on your control node (the computer that will run the playbooks). You do not need to install anything on the other nodes except Python and ssh!

To learn how to install Ansible, click [here](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#control-node-requirements).

## Creating your inventory

The inventory is probably the most central piece in your Ansible flow, it is the file that lists all the remote hosts you want to access as well as variables to help it connect to them or configure them. To know all there is to know about Ansible inventory check [this link](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html).

Here we will only look at the basics but I hope it will serve you well!