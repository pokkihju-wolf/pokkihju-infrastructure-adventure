# Getting a Domain Name from OVH

OVH offers, amongst other things to buy a domain name.

## Ordering a Domain Name

Just go to the OVH website, select Domain Names and buy the one that interests you. They will also probably offer you alternatives should it already be taken.

## My Choice

I have bought the following domain names:
- pokkihju.me
- pokkihju.fr

(And others that I will not publish here)

## Issues

As I was trying to get my Let's Encrypt SSL certificate, I encountered an issue on OVH. There is no easy way that I found or any documentation
about this hypothetical way to get a certificate. The only way would be to get a hosting plan (which is not what I want) or generate the certificate
manually. So I guess I will have to do that and you will get a tutorial on there.

## How to direct your domain name to your server

Simply change/add (depending on if you're adding a subdomain or just modifying one) an A type entry with the domain name under "DNS Zone" and your server IP as a target.

Any subdomain you want to generate a certificate for has to be set in your DNS so you might want to add the wildcard catch-all subdomain, it will look like this: ```*.example.com```.

## Additionnal Info

You might need to prove your identity as a domain name will be registered to YOU. So be prepared and get the documents ready.