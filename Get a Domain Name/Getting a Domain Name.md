# How to get a Domain Name

A Domain Name is a way to identify your server to the world. Once you have configured your domain name, anyone trying to reach it should reach your server. As such, you will most likely need one if you want to have a real website.

In order to get a domain name, you will most likely need to buy it from a provider or a domain name seller. There are many, and you can find a lot of them through a simple Google search.

List of Domain name providers I have used:
- OVH