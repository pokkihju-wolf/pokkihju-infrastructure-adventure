# Setting up terraform scripts

Hey everyone!

If you are here, it means that you probably already have a private cloud set up or that you plan to use a public cloud based on Openstack.
(Yes only Openstack provider will be mentionned here as this is what I use, there are plenty more tutorials for other cloud providers).

Now that everyone is on the same page, let's start!

## Installing terraform

If you want to install terraform, I advise you to look into the [terraform documentation](https://developer.hashicorp.com/terraform/install).

While you're here, I advise you to look into OpenTofu, it is an Open Source fork of terraform that was created following to closing of terraform
source code by HashiCorp, to install it, look here: [OpenTofu documentation](https://opentofu.org/docs/intro/install/)

## Creating your provider

Now that you have installed either terraform or opentofu, you can create your first .tf file. I often name mine provider.tf as that is what will be in the file. Here is a look at the content:

```
# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

variable "APPLICATION_CREDENTIAL_ID" {
  type = string
}

variable "APPLICATION_CREDENTIAL_SECRET" {
  type = string
}

# Configure the OpenStack Provider
provider "openstack" {
  application_credential_id     = var.APPLICATION_CREDENTIAL_ID
  application_credential_secret = var.APPLICATION_CREDENTIAL_SECRET
  auth_url = "https://your.domain.tld/openstack-keystone/v3"
  region   = "RegionOne"
  endpoint_overrides = {
    "network" = "https://your.domain.tld/openstack-neutron/v2/"
    "image"   = "https://your.domain.tld/openstack-glance/v2/"
    "compute" = "https://your.domain.tld/openstack-nova/v2/"
    "volumev3" = "https://your.domain.tld/openstack-cinder/v3/"
  }
}
```

As you can see, there is a lot of information there and not all of it is well organised (yes I should and will probably refactor it soon).

Let's take a closer look

```
# Define required providers
terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}
```

This block right here is the first one and it is also very important. It defines the required providers for the project. Here, we define that we want the openstack provider and we want it with a version at least 1.53.0. This is important as there might be changes depending on the provider version. Make sure that this version matches what you want to do with it.

If you want more information on the openstack terraform provider, look [here](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs).

Once we have set our requirements, we set two variables:

```
variable "APPLICATION_CREDENTIAL_ID" {
  type = string
}

variable "APPLICATION_CREDENTIAL_SECRET" {
  type = string
}
```

These are the application credentials ID and secret. You will need these for terraform to be able to access the API and create your infrastructure. Be VERY CAREFUL with these as anyone who has access to them can use them to create, destroy and change in many ways your infrastructure. NEVER STORE THEM IN VERSIONNED FILES AND OTHERWISE PUBLICLY ACCESSIBLE FILES.

Once that is done, we then create the actual provider that we will use:

```
# Configure the OpenStack Provider
provider "openstack" {
  application_credential_id     = var.APPLICATION_CREDENTIAL_ID
  application_credential_secret = var.APPLICATION_CREDENTIAL_SECRET
  auth_url = "https://your.domain.tld/openstack-keystone/v3"
  region   = "RegionOne"
  endpoint_overrides = {
    "network" = "https://your.domain.tld/openstack-neutron/v2/"
    "image"   = "https://your.domain.tld/openstack-glance/v2/"
    "compute" = "https://your.domain.tld/openstack-nova/v2/"
    "volumev3" = "https://your.domain.tld/openstack-cinder/v3/"
  }
}
```

This is normally easier but here I must override all of the endpoints as microstack uses slightly different endpoint names compared to the regular openstack. Note that you can also just download the open rc file from your cloud provider if you use a public one and source it before running terraform, it should do the trick.

As you can see we use the previously defined variables to set the credentials for the API which lets us avoid storing them in plain text in the provider.tf file.

## Creating a simple instance

This here will just show you how to create a simple instance. Depending on the provider you might have a few issues, if that is the case, reach out by opening an issue on this github repo and I will try to update the document as quickly as possible to include a fix for your issue.

```
resource "openstack_compute_instance_v2" "instance_1" {
  name = "instance_name"

  image_name  = "theimagenameyouwant"
  flavor_name = "flavor_name"
}
```

As you can see this is very minimal. This creates a simple instance with the name "instance_name" and the image "theimagenameyouwant". The flavor is the size of the VM. If you're using microstack and it has been populated with defaults you can use m1.tiny which creates a VM with 512MB of RAM and 1 vCPU.

To get the image names or ids and flavor names or ids, you can either use the openstack cli or look them up on your horizon dashboard.

Please note that image_name and flavor_name can be replaced respectively by image_id and flavor_id. More documentation on this resource can be found [here](https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_instance_v2#argument-reference).