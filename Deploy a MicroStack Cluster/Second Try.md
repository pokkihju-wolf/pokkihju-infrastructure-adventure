# My second try deploying a MicroStack cluster on my server

So, I noticed after my first try that the cluster did not survive well after a reboot of my server and that I had not deployed storage on the cluster.

So, first step is uninstalling the last cluster, for that:
[Teardown tutorial](https://discourse.ubuntu.com/t/tear-down-your-openstack-lab-environment/25078)

Once that is done, let's start.

## First steps

So I will not follow any of the microstack tutorials as they do not mention the storage role directly except on the multi node tutorial. First steps are the same however so until the sunbeam cluster bootstrap command you can follow any of them: [microstack tutorials](https://microstack.run/docs/single-node-guided)

Now, here is the command I have run afterwards: ```sunbeam cluster bootstrap --role control --role compute --role storage```

### First Issue

So, the first issue I had is that I needed to use a virtual disk for my microceph back as I only had one disk on my server. If you have multiple disks and can dedicate an entire disk to microceph you will not encounter this issue.

The issue I encountered is that in order to create this virtual disk, I followed this tutorial with some changes: [microk8s with micro-ceph](https://discuss.kubernetes.io/t/howto-setup-microk8s-with-micro-ceph-storage/25043).

However, I encountered this bug when trying to add the disk: [Github Issue](https://github.com/canonical/microceph/issues/251).

After following the described fix however, this issue was resolved. The fix is as follows for those too lazy to click on the link:

- Edit the /var/lib/snapd/apparmor/profiles/snap.microceph.daemon
- Add this line wherever you feel it should go: ```/dev/sd{,[a-z]}[a-z][0-9]{,[0-9],[0-9][0-9]} rwk, # SCSI rule!```
- run ```sudo apparmor_parser -r /var/lib/snapd/apparmor/profiles/snap.microceph.daemon```

Note that I had issues removing the loop devices so here is a quick tutorial on how to remove a loop device:

```losetup -d /dev/loopXX```

You might need to sudo this command.

If you want to see all loop devices, you can use: ```lsblk```

### Second Issue

My second issue was a bit more complex in that it was (it seems) a compatibility issue between microceph and the rest of the components.
It appears that when installing microceph, sunbeam pulls the latest/edge channel, which might introduce issues and incompatibilities. You can see the bug I opened here: [my launchpad bug report](https://bugs.launchpad.net/snap-openstack/+bug/2062993)

In the end, in order to fix that, I ran the following command:

```
sunbeam -v cluster bootstrap --manifest /snap/openstack/current/etc/manifests/candidate.yml --role control --role compute --role storage
```

Then it breaks because of the virtual disk, then I run the fix from the first issue and run the command again. Some issues from my first try deploying the cluster might occur so I encourage you to go look at the first try here: [First Try deploying a Microstack cluster](https://gitlab.com/pokkihju-wolf/pokkihju-infrastructure-adventure/-/blob/main/Deploy%20a%20MicroStack%20Cluster/First%20Try.md?ref_type=heads)

## Configure

For the configure step, I just the ```sunbeam configure``` command.

### Yet another issue

I found out while running the configure command that my microceph backed services did not work. That was of course a major issue for me.

After trying to understand what was going on I gave up and just completely reinstalled microceph and readded the integrations with microstack myself.

Here is the "script" I used:

```
# Remove relations of microceph
juju remove-relation -m openstack microceph:ceph cinder-ceph:ceph
juju remove-relation -m openstack microceph:ceph glance:ceph

# remove microceph offer
juju remove-offer microceph

# remove microceph saas
juju remove-saas -m openstack microceph

# remove microceph application
juju remove-application microceph --force --no-wait

# remove microceph snap
sudo snap remove --purge microceph

# deploy microceph
juju deploy microceph --channel reef/candidate --to 0

# Add storage to microceph manually through juju storage
juju add-storage microceph/X osd-standalone='loop,200G,3'

# recreate microceph offer
juju offer microceph:ceph

# re integrate microceph to openstack
juju integrate -m k8s glance:ceph admin/controller.microceph
juju integrate -m k8s cinder-ceph:ceph admin/controller.microceph
```

### You thought it was over ?

So, after a few days (I did not use the infrastructure during that time) I noticed that while my cluster seemed healthy, the ceph cluster was anything but. First, I had added many disks that were ignored and second, most disks were in a constant state of up and down and none were up for a long time.

The fix for this was in the end quite simple: I removed every disk that was not in the pool. Once that was done, the remaining disks started working normally. In order to do that, you will need to first remove the disk from ceph and microceph and then delete/disassociate them from the service. Otherwise you risk breaking the microceph cluster.

## Useful links or commands

### Microceph page: 
https://charmhub.io/microceph