# My First try deploying a MicroStack cluster on my server

In order to deploy a MicroStack cluster on a single server, you need to follow one of the two following guides:
- [basic single node documentation](https://microstack.run/docs/single-node)
- [advanced single node documentation](https://microstack.run/docs/single-node-guided)

These are supposed to be easy and quick.

In my personnal experience they are anything but easy as any issue you may have is hard to troubleshoot as there is little documentation and you will need to dive deep into juju, sunbeam and kubernetes to identify and fix the issue.
However, MicroStack remains easier to deploy than just deploying openstack services individually.

One tutorial you will very probably need is this one:
- [Teardown tutorial](https://discourse.ubuntu.com/t/tear-down-your-openstack-lab-environment/25078)

It has been vital for me as it allows you to completely remove the cluster and start over as if nothing had ever happened.

## First steps

So, I followed the Single Node guide, as I will use another method (Nginx) to handle access from the internet to the VMs.

So I launch the ```sunbeam cluster bootstrap --accept-defaults``` command and... nothing.

I wait and wait and I am stuck on the line that says that it is waiting for the openstack control plane services to be ready.

## Analyzing

After the timeout, trying to understand what is going on I use the following command to inspect what is going on: ```juju status -m openstack```.

Remember this command, it is VERY useful. So, once I had this status I noticed that I had two units with workload in "blocked" state with messages saying something had timed out. However, no matter how hard I looked, no more info. So I went directly into the charm and found that it was the db sync that had timed out. However, I found no way to restart the process so I was stuck.

In order to have more details on the charm logs I used:

```kubectl -n openstack logs pods/<application>-0 -c charm```

## Bypass

After thinking on this a lot, I noticed in the charm that the db sync step depended on having all relations ready.
I then decided to try to remove one of these relations and then adding it back, hoping to trigger the step that had timeout before and also hoping that it worked this time. In order to do that I used the following commands:

```
juju remove-relation -m openstack <application1>:<relationType> <application2>:<relationType>
juju integrate -m openstack <application1>:<relationType> <application2>:<relationType>
```

This, fortunately, was enough to fix the cluster and I was then able to run the final commands in the tutorial.

## Final commands

So, the final commands of the Single Node tutorial, I had no issues with.

```sunbeam configure --accept-defaults --openrc demo-openrc```

The above command worked as advertised and I was able to launch an ubuntu instance using ```sunbeam launch ubuntu --name test``` easily.

## The END

So that was my first try to deploy the microstack cluster. Why first try you might ask, well because I noticed afterwards that in default mode, only compute and control. However, I also needed storage. So... Guess I am good to go another round.