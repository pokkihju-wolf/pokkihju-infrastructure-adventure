# Getting a server from OVH

OVH is a cloud provider that has a wide range of baremetal offerings. The reason I chose it as my first server host is that its "ECO" range is quite good for the price and they were the least costly to match my requirements. However they are not configurable so if you have very specific needs they may not match.

OVH offers multiple ranges of server:

Eco:
- Kimsufi (Great for very small servers or self hosting some small game server)
- So you Start (A bit more costly, but with a much wider range of capacity)
- Rise (Much better perf than the others but also much more costly so do with that what you will)

Dedicated standard:
- Advance (Quite configurable and great offer but costly if you are not a company or have a good fun budget)
- Game (For gaming obviously, although they are high performange gaming servers and often overkill unless you want to host a 1000 slot minecraft modded server)

There are other servers offered but I am completely unqualified to speak about them so I will not do so.

## Ordering a server

Just choose your configuration, pay, and wait for it to be ready. Once it is ready install the OS of your choice and you're good to go.

## Installing an OS on the server

If you're installing an OS on the server using an OVH template, you can modify the partitions and have them split on the disks if you have RAID enabled. That is what I did to split my volumes for cinder, my images for glance and my object storage partition (though I have yet to install this specific element on the openstack server).

## My choice

Personnaly I have chosen the SYS-6-SAT-128 (So you Start) as my server as it was a good price and matched quite well what I wanted to do with it.