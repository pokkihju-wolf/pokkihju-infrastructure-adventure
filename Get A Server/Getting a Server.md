# How to get a Server

A lot of the time, personnal projects need to be deployed, otherwise what's the point ? Sure you can absolutely just write a webapp for fun but isn't it better to see it on the world wide web ?
You may also want to host your friend group gaming server and manage it yourself!

In this section we will explore multiple ways to get a server for yourself (and/or a friend group or small company).

Please note that you can always deploy your application on a cloud kubernetes cluster, or many other ways, this section is only for those interested in getting a server for their personnal projects and needs.

For now I have tested the following providers:
- OVH