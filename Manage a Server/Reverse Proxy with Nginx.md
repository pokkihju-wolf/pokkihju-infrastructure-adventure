# How to setup a reverse proxy with Nginx

In here, I will explain how to setup an NGinx server using docker, docker-compose and a config file. Please note that this NGinx is only set to do reverse proxy and not serve anything.

## Step 1: Prepare the NGinx.conf file

The configuration presented here will only do one thing: proxy traffic from the public IP of your server to the microstack API.

!!! THIS IS DANGEROUS, ESPECIALLY IF YOU HAVE NOT IMPLEMENTED STEP 3 !!!!

!!! DO NOT DO THIS ON ANY ENTERPRISE PRODUCTION ENVIRONMENT !!!

Here is the configuration file, it is VERY simple, I will explain it quickly but if you want more customization look in [the beginner's guide](https://nginx.org/en/docs/beginners_guide.html) or the rest of the NGinx documentation.

```
events {
        worker_connections 1024;
}

http {
        server {
                listen 80;
                location / {
                        proxy_pass http://10.20.21.12/;
                }
        }
}
```

So, first things first, events here only sets the number of worker connections that the server can handle, it is set to 1024 but depending on your needs you might want to scale that up or down.

The second part is the http block. This handles http reverse proxying. Once again, this one is very simple, it redirects any http traffic arriving on port 80 to the local ip 10.20.21.12 (which on my server matches the microstack reverse proxy that handles connections to the API).

## Step 2: Prepare the compose.yaml file

Once again, this compose file is very simple. It only handles the setup for the nginx server. If you want more info, go to [the compose file documentation](https://docs.docker.com/compose/compose-file/compose-file-v3/).

```
version: "3.8"

services:
  nginx:
    image: nginx
    volumes:
      - type: bind
        source: ./nginx.conf
        target: /etc/nginx/nginx.conf
        read_only: true
    ports:
      - "80:80"
```

So, version indicates the compose file version, useful if you are using old deprecated version of docker compose.

Services list all the services the compose file handles, note that here, we only have nginx.

In the nginx service we specify that we are using the nginx image, best practice would have us use a version tag so as not to constantly change versions but this is a quick and dirty setup, you can always change that later.

For the volumes, there is only one and it is a local binding, this means that the conf file must be in the same folder as the compose file. You could also give an absolute path on your machine if you want. The target however must not change as it is the default conf the server will use in the container. I have set the conf file as read only but you can skip that if you want, it is only for security purposes.

As you can see in the ports section, we are binding port 80 to port 80, this means that our container will listen to port 80 on all server interfaces however so you might want to specify the IP here.

## Step 3: Setup TLS

In this section, I will explain how to setup TLS on your server.

In reality this is very easy. But first, I direct you to the Generate certificates tutorial in order to create a certificate. Once that is done, you can come back here.

Okay, so you have a certificate, now you will want to add it in your container. Update your conf like this:

```
version: "3.8"

services:
  nginx:
    image: nginx:1.14.2
    volumes:
      - type: bind
        source: ./nginx.conf
        target: /etc/nginx/nginx.conf
        read_only: true
      - type: bind
        source: /path/to/my/cert/fullchain.pem
        target: /ssl/certificates/certificate.pem
        read_only: true
      - type: bind
        source: /path/to/my/cert/key.pem
        target: /ssl/certificates/key.pem
        read_only: true
    ports:
      - "80:80"
```

Now that this is done, you can update your nginx conf file.

First we will update the listen line:

```
- line => old line
+ line => new line
```

```
events {
        worker_connections 1024;
}

http {
        server {
-               listen 80;
+               listen 443 ssl;
                location / {
                        proxy_pass http://10.20.21.12/;
                }
        }
}
```

We changed two things, first, by setting port as 443, we are using the standard https port. Then, by using the ssl directive we are indicating to nginx that we want to activate ssl. But our conf update is not done. Next, we will indicate our domain name.

```
events {
        worker_connections 1024;
}

http {
        server {
                listen 443 ssl;

+               server_name mydomain.tld;

                location / {
                        proxy_pass http://10.20.21.12/;
                }
        }
}
```

This will tell nginx what is its name. This is important as this must match one of the names used on the certificates otherwise you will have an error on the browser side as the browser will indicate that the certificates does not match the domain name.
Note that this will also force people to use your domain name as directly hitting on the IP will no longer directly match the nginx server. Anyone doing that will receive a 404 error.

Next we are adding the certificate and key:

```
events {
        worker_connections 1024;
}

http {
        server {
                listen 443 ssl;

                server_name mydomain.tld;

+               ssl_certificate     /ssl/certificates/certificate.pem;
+               ssl_certificate_key /ssl/certificates/key.pem;

                location / {
                        proxy_pass http://10.20.21.12/;
                }
        }
}
```

You will notice that this matches the bind points from our compose file. If this does not match, nginx will not find the cert files and thus will not start.

Once we have our certificate and key we are almost done, we will only add two lines:

```
events {
        worker_connections 1024;
}

http {
        server {
                listen 443 ssl;

                server_name mydomain.tld;

                ssl_certificate     /ssl/certificates/certificate.pem;
                ssl_certificate_key /ssl/certificates/key.pem;
+               ssl_protocols       TLSv1.1 TLSv1.2 TLSv1.3;
+               ssl_ciphers         HIGH:!aNULL:!MD5;

                location / {
                        proxy_pass http://10.20.21.12/;
                }
        }
}
```

These lines specify that we only accept TLS versions greater than 1 (mostly for security) and specifies the ciphers accepted.

One last modification to the compose file and we're done:

```
version: "3.8"

services:
  nginx:
    image: nginx:1.14.2
    volumes:
      - type: bind
        source: ./nginx.conf
        target: /etc/nginx/nginx.conf
        read_only: true
      - type: bind
        source: /path/to/my/cert/fullchain.pem
        target: /ssl/certificates/certificate.pem
        read_only: true
      - type: bind
        source: /path/to/my/cert/key.pem
        target: /ssl/certificates/key.pem
        read_only: true
    ports:
-     - "80:80"
+     - "443:443"
```

We are now listening on port 443 so that is the port that we must map on our nginx server.

Congratulations, your nginx now reverse proxies on HTTPS!

## Step 4: Run

To run once everything is done, simply use ```docker-compose up -d``` in your folder.

## Step 5 (optionnal): Serve static files directly from nginx

This step will teach you how to serve files directly from your Nginx server. This can be useful in many cases so I will mention it here.

In order to do that, you will need to add this line in your http context in your conf:

```
        include mime.types;
```

This line is not mandatory but without it, nginx won't recognize and properly index content like css and such so you might have some small issues.

Once that is done, create your server block:

```
server {
        listen 80,

        server_name myserver.tld;
}
```

Afterwards we will add the root of our documents:

```
        root /www/data;
```

This is the path in the container or server where you will have stored the static pages or files that you wish to present to the world.

Then you can finally add the location:

```
        location / {
                autoindex on;
        }
```

And small bonus if you wish to remove the .html part of your static html files you can add that line in the location context:

```
        try_files $uri.html $uri $uri/ /404.html;
```

where /404.html is the path to go to in case everything else fails.

And you're good to go, you are now serving static files from your nginx server.

If you're using docker compose, do not forget to map the files you want to server from your machine to the actual server through a volume, otherwise this will not work.

You can do so like this:

```
  nginx:
    image: nginx
    volumes:
      - type: bind
        source: ./nginx.conf
        target: /etc/nginx/nginx.conf
        read_only: true
      - type: bind
        source: /path/to/static/files
        target: /path/to/root/set/in/conf
        read_only: true
    ports:
      - "80:80"
```