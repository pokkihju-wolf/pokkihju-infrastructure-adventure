# How to generate a certificate for a domain name pointing to your server using certbot

Please note that this tutorial is mostly copied from [this](https://certbot.eff.org/instructions?ws=other&os=ubuntufocal) page. You can also find tutorials for other OS on there. For wildcard domains, follow [this link](https://certbot.eff.org/instructions?ws=other&os=ubuntufocal&tab=wildcard).

## Step 1: install certbot

On ubuntu 22.04 Jammy Jellyfish, you can install certbot using snap:

```sudo snap install --classic certbot```

Then you will want to create a symlink to ensure you can use the certbot command:

```sudo ln -s /snap/bin/certbot /usr/bin/certbot```

Once that is done, you can check it is installed by running:

```certbot --version```

## Step 2: generate the certificate (for standard domain name)

First, make sure you have set up the DNS of all domain names you want a certificate to point to your server.

Now you will run the command:

```sudo certbot certonly --standalone```

I used this command because I setyp my nginx on docker so certbot could not access it.

Once that is done, input the information that the certbot asks of you and if you have setup your DNS properly, you should have a certificate. You can even have the certificate cover multiple domain names as long as you input them separated by a space and/or a comma.

You will be able to find your certificate in /etc/letsencrypt/yourdomainname.

## Step 2.5: generate the certificate (for wildcard domain)

First, make sure you have set up the DNS of all domain names you want a certificate to point to your server.

Then, depending on your dns provider, install the certbot dns plugin ov your DNS provider if it exists, otherwise it is a bit more complex.

This part is only for ovh. Generate an API token on the [ovh api](https://www.ovh.com/auth/api/createToken) with permissions for all methods on /domain/zone/*.
Once that is done, create an ovh.ini file that looks like this:

```
dns_ovh_endpoint = ovh-eu
dns_ovh_application_key = <your-app-key>
dns_ovh_application_secret = <your-app-secret>
dns_ovh_consumer_key = <your-app-consumerkey>
```

Then, you can run the command:

```certbot certonly --dns-ovh --dns-ovh-credentials /path/to/ovh.ini```

And fill in the domains you want to have on the cert. Please note that you will need to have these domain on OVH or whatever provider you are using.

Then you have your certificates, congratulations!