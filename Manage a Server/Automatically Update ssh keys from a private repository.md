# How to Automatically update your authorized keys from a private gitlab repository.

This question came to me when I wanted to share my server with a friend. I was wondering how I could do that without having to add the key manually.

Then I thought, hey, this might also be useful to add a key from another of my computers. So I set to create a small bash script to do that.

## The Script

The script that you will find below is my final script, You may think "hey, I have a better way of doing this" and you probably would be right. I did this in 10mn without really trying to write the cleanest code. But let's try to explain it.

First we have the logging lines (yes I know I should have written a function but I was too lazy so only redirects for now). The only purpose of these lines is to log which keys were added, and when.
```
echo "$(date): Running access-keys update" >> /tmp/access-keys-update.log
...
        echo "Checking $line ..." >> /tmp/access-keys-update.log
...
                echo "Adding $line to authorized_keys." >> /tmp/access-keys-update.log
...
echo "Done adding new keys. Added $count keys to authorized_keys" >> /tmp/access-keys-update.log
```

Then we have the first 3 real lines of script, whose only purpose is to pull the repository and then go back to the directory above (to avoid polluting the local repository copy).
```
cd /path/to/home/keys/server-access-keys

git pull

cd ..
```

After that comes the first "interesting" line of code. It lists the files in the repository before filtering to keep only .pub files and adding them to a file named 'ssh-keys-list.txt' using grep and ls. This is quite a basic line but it might be of use to you to know how to do that.
```
ls server-access-keys | grep ".pub" > ssh-keys-list.txt
```

Then we set a count of how many keys we added to 0 (If you can not read this line, please take a basic bash course, it might be of use to you).

After that is the main part of the script, the loop.

The first important part there is that we are using the file created above ('ssh-keys-list') as an input and reading it line by line (that is the default behaviour of read if I am not mistaken).

Once that is done we try to find the content of the key named on the line of 'ssh-keys-list' in our authorized keys file.

We then check the 'contains.txt' file. If it is not empty, it means we already have the key so no need to add it. Otherwise we add it and increase the count of added keys by one.

To end each loop, we remove the contains.txt to ensure it does not stay afterwards.

```
while read line; do
        cat /path/to/home/.ssh/authorized_keys | grep "$(cat server-access-keys/$line)" > contains.txt
        if [ ! -s contains.txt ]; then
                cat "server-access-keys/$line" >> /path/to/home/.ssh/authorized_keys
                count=$((count+1))
        fi
        rm contains.txt
done < ssh-keys-list.txt
```

Once that is done, all we have left to do is cleanup by removing the 'ssh-keys-list.txt' file:

```
rm ssh-keys-list.txt
```

And we're done. Below the full script:

```
#!/bin/bash

echo "$(date): Running access-keys update" >> /tmp/access-keys-update.log

cd /path/to/home/keys/server-access-keys

git pull

cd ..

ls server-access-keys | grep ".pub" > ssh-keys-list.txt

count=0

while read line; do
        echo "Checking $line ..." >> /tmp/access-keys-update.log
        cat /path/to/home/.ssh/authorized_keys | grep "$(cat server-access-keys/$line)" > contains.txt
        if [ ! -s contains.txt ]; then
                cat "server-access-keys/$line" >> /path/to/home/.ssh/authorized_keys
                count=$((count+1))
                echo "Adding $line to authorized_keys." >> /tmp/access-keys-update.log
        fi
        rm contains.txt
done < ssh-keys-list.txt

rm ssh-keys-list.txt

echo "Done adding new keys. Added $count keys to authorized_keys" >> /tmp/access-keys-update.log
```

## How to run it periodically

In order to run the script periodically (every hour) I am using cron jobs. They are easy to configure and very effective.

To add a new cron job, use ```crontab -e```. That will open the crontab file that lists all of the crons that run on your machine. Then, add a line:

```
mn h d mo w /path/to/your/script.sh
```

mn is the minutes at which your script will run. For example: ```10 * * * * /script.sh``` will run ```script.sh``` every hour at the tenth minute, so, at 00:10, 01:10, etc.
You can also do something every 30 minutes: ```*/30 * * * * /script.sh```.
There are many possibilities and I encourage you to check out the [documentation](https://doc.ubuntu-fr.org/cron). It might help you understand this better.

Personnaly I have run my script every hour at the 30th minute so ```30 * * * * /path/to/my/script```.