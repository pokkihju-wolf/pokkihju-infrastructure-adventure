# How to install Placement

Placement is the resource tracking service of Openstack. All services can register their resource in it, though its main usage is for Nova resource tracking. It is required for Nova to work so it must go before Nova in your installation order.

## Installing on an Ubuntu server

In order to install Placement on an Ubuntu server it is recommended to follow the [procedure given on the Openstack website.](https://docs.openstack.org/placement/2024.1/install/install-ubuntu.html)

As with every one of my install I have created scripts:

### Install placement

```
#!/bin/bash

# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING PLACEMENT DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create placement database and grant access to placement user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS placement; CREATE DATABASE placement; GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/placement_db.pass)'; GRANT ALL PRIVILEGES ON placement.* TO 'placement'@'%' IDENTIFIED BY '$(cat $HOME/secrets/placement_db.pass)'"

# Placement user and service setup

# 0 - Source the admin credentials created while installing keystone
source $HOME/open-rcs/admin-openrc.sh

# 1 - Create the placement user
openstack user create --domain default --password $(cat $HOME/secrets/placement.pass) placement

# 2 - Add admin role to placement on service project
openstack role add --project service --user placement admin

# 3 - Create placement service entity
openstack service create --name placement --description "Placement API" placement

# 4 - Create service API endpoints
openstack endpoint create --region RegionOne placement public http://controller:8778
openstack endpoint create --region RegionOne placement internal http://controller:8778
openstack endpoint create --region RegionOne placement admin http://controller:8778

# Installing the service package
sudo apt install placement-api -y
# Ensure Apache2 is present
sudo apt install apache2 -y

# Removing old/default conf
sudo rm /etc/placement/placement.conf

# Copying the correct conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/placement/placement.conf /etc/placement/placement.conf
# Replacing the db password
sudo sed -i "s/DB_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/placement_db.pass)/g" /etc/placement/placement.conf
# Replacing the placement user password
sudo sed -i "s/PASSWORD_TO_REPLACE/$(cat $HOME/secrets/placement.pass)/g" /etc/placement/placement.conf

# Populate placement DB
sudo su -s /bin/sh -c "placement-manage db sync" placement

# Restart apache2
sudo service apache2 restart
```

This script uses the same general blocks as glance and as most other services. First we create the user, service and endpoints in Keystone, then install the package, and then replace the conf with the passwords from the home secrets folder before syncing the DB and restarting the service.

### Uninstall placement

```
#!/bin/bash

# Source credentials
source $HOME/open-rcs/admin-openrc.sh

# Remove placement user
openstack user delete placement

# Remove placement service
openstack service delete placement

# Remove the package
sudo apt remove -y placement-api

# Remove the old conf
sudo rm /etc/placement/placement.conf
```

This script is very simple, delete user and service from keystone and then remove the package and old conf.
