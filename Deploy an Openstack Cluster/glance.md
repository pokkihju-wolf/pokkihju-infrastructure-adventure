# How to Install Glance

Glance is the image service of Openstack, it is used to store base and saved images for later use in your compute deployments. It is thus important to deploy it *before* Nova and Placement. 

## Installing on an Ubuntu server

In order to install Glance on an Ubuntu server it is recommended to follow the [procedure given on the Openstack website.](https://docs.openstack.org/glance/2024.1/install/install-ubuntu.html)

I have, as with Keystone, created scripts for this service:

### Install glance

```
#!/bin/bash

# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING GLANCE DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create glance database and grant access to glance user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS glance; CREATE DATABASE glance; GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/glance_db.pass)'; GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'%' IDENTIFIED BY '$(cat $HOME/secrets/glance_db.pass)'"

# Glance user and service setup

# 0 - Source the admin credentials created while installing keystone
source $HOME/open-rcs/admin-openrc.sh

# 1 - Create the glance user
openstack user create --domain default --password $(cat $HOME/secrets/glance.pass) glance

# 2 - Add admin role to glance on service project
openstack role add --project service --user glance admin

# 3 - Create glance service entity
openstack service create --name glance --description "Openstack Image" image

# 4 - Create service API endpoints
openstack endpoint create --region RegionOne image public http://controller:9292 | grep "| id" | sed -E 's/^\|\ id\ *\|\ (([0-9]|[a-z])+)\ \|/\1/' >$HOME/public_glance_id
openstack endpoint create --region RegionOne image internal http://controller:9292
openstack endpoint create --region RegionOne image admin http://controller:9292

# 5 - Register quota limits
openstack registered limit create --service glance --default-limit 4000 --region RegionOne image_size_total
openstack registered limit create --service glance --default-limit 2000 --region RegionOne image_stage_total
openstack registered limit create --service glance --default-limit 100 --region RegionOne image_count_total
openstack registered limit create --service glance --default-limit 100 --region RegionOne image_count_uploading

# Installing the service package
sudo apt install glance -y

# Removing old/default conf
sudo rm /etc/glance/glance-api.conf

# Copying the correct conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/glance/glance.conf /etc/glance/glance-api.conf
# Replacing the db password
sudo sed -i "s/DB_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/glance_db.pass)/g" /etc/glance/glance-api.conf
# Replacing the glance user password
sudo sed -i "s/PASSWORD_TO_REPLACE/$(cat $HOME/secrets/glance.pass)/g" /etc/glance/glance-api.conf
# Replacing the endpoint id for glance
sudo sed -i "s/ENDPOINT_ID_TO_REPLACE/$(cat $HOME/public_glance_id)/g" /etc/glance/glance-api.conf

# Cleanup the public_glance_id file
rm $HOME/public_glance_id

# Add reader role to glance for all resources
openstack role add --user glance --user-domain Default --system all reader

# Populate glance db
sudo su -s /bin/sh -c "glance-manage db_sync" glance

# Restart glance
sudo service glance-api restart
```

This script has quite a lot of steps, some of which could probably be compressed as they will have to be done for every service following keystone:

```
# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING GLANCE DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create glance database and grant access to glance user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS glance; CREATE DATABASE glance; GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/glance_db.pass)'; GRANT ALL PRIVILEGES ON glance.* TO 'glance'@'%' IDENTIFIED BY '$(cat $HOME/secrets/glance_db.pass)'"

# Glance user and service setup
# 0 - Source the admin credentials created while installing keystone
source $HOME/open-rcs/admin-openrc.sh

# 1 - Create the glance user
openstack user create --domain default --password $(cat $HOME/secrets/glance.pass) glance

# 2 - Add admin role to glance on service project
openstack role add --project service --user glance admin

# 3 - Create glance service entity
openstack service create --name glance --description "Openstack Image" image

# 4 - Create service API endpoints
openstack endpoint create --region RegionOne image public http://controller:9292 | grep "| id" | sed -E 's/^\|\ id\ *\|\ (([0-9]|[a-z])+)\ \|/\1/' >$HOME/public_glance_id
openstack endpoint create --region RegionOne image internal http://controller:9292
openstack endpoint create --region RegionOne image admin http://controller:9292
```

All of these commands will have to be run, of course, each service has its user name and service name but they each have to create the endpoints, the service entity, the user and be given admin role on the service project. They also each require their own DB (except for horizon but this is a special case).

The rest of the script is mostly just installing the package, copying the conf and replacing placeholders within though you might notice that there is one placeholder that is the public endpoint id, this is quite specific to glance so it is required to extract it earlier. One other specificity of glance is the need for its user to have a full reader role access to the system.

### Init glance

```
#!/bin/bash

# Source admin credentials
source $HOME/open-rcs/admin-openrc.sh

# Download images
cd $HOME/images
wget http://download.cirros-cloud.net/0.6.2/cirros-0.6.2-x86_64-disk.img
wget https://repo.almalinux.org/almalinux/9/cloud/x86_64/images/AlmaLinux-9-GenericCloud-9.5-20241120.x86_64.qcow2
wget https://cloud.debian.org/cdimage/cloud/OpenStack/10.13.27-20240701/debian-10.13.27-20240701-openstack-amd64.qcow2
wget https://download.fedoraproject.org/pub/fedora/linux/releases/41/Cloud/x86_64/images/Fedora-Cloud-Base-Generic-41-1.4.x86_64.qcow2
wget https://dl.rockylinux.org/pub/rocky/9/images/x86_64/Rocky-9-GenericCloud-Base.latest.x86_64.qcow2
wget https://cloud-images.ubuntu.com/noble/current/noble-server-cloudimg-amd64.img

# Create images in glance
glance image-create --name "cirros" \
  --file cirros-0.6.2-x86_64-disk.img \
  --disk-format qcow2 --container-format bare \
  --visibility=public

glance image-create --name "ubuntu-noble" \
  --file noble-server-cloudimg-amd64.img \
  --disk-format qcow2 --container-format bare \
  --visibility=public

glance image-create --name "rocky9" \
  --file Rocky-9-GenericCloud-Base.latest.x86_64.qcow2 \
  --disk-format qcow2 --container-format bare \
  --visibility=public

glance image-create --name "Fedora41" \
  --file Fedora-Cloud-Base-Generic-41-1.4.x86_64.qcow2 \
  --disk-format qcow2 --container-format bare \
  --visibility=public

glance image-create --name "debian10" \
  --file debian-10.13.27-20240701-openstack-amd64.qcow2 \
  --disk-format qcow2 --container-format bare \
  --visibility=public

glance image-create --name "Alma95" \
  --file AlmaLinux-9-GenericCloud-9.5-20241120.x86_64.qcow2 \
  --disk-format qcow2 --container-format bare \
  --visibility=public
```

As with keystone init, this script is quite simple, just get the credentials, download the images and push them to glance.

### Uninstall glance

```
#!/bin/bash

# Source credentials
source $HOME/open-rcs/admin-openrc.sh

# Remove glance user
openstack user delete glance

# Remove glance service
openstack service delete glance

# Remove the package
sudo apt remove -y glance

# Remove the old conf
sudo rm /etc/glance/glance-api.conf
```

This script is really the simplest of the three, we delete the user and service glance and then remove the package and the conf.