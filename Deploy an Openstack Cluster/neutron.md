# How to Install Neutron

Neutron is the networking service of Openstack. It works alongside Nova to provide connection between VMs themselves and also between VMs and external networks.

## Installing on an Ubuntu Server

As for Nova, there are 2 parts in a Neutron installation:
- [The control node installation](https://docs.openstack.org/neutron/2024.1/install/controller-install-ubuntu.html)
- [The compute node installation](https://docs.openstack.org/neutron/2024.1/install/compute-install-ubuntu.html)

There are also some specificities due to the way Neutron works. Here, I will use my second interface on my server to handle traffic to and from VMs. Previously I had tried to do so using the public interface but while it worked, it seemed to have some issues and as I am doing this on a single node, it is better to handle the traffic on a free interface that sees 0 traffic in my opinion (of course you are free to disagree and do it another way).

There will be an install script and an uninstall script. Be ready for them to be a bit more complex as they will also touch on OpenVSwitch setup and that is a bit more complex than a basic Openstack service.

### Install Neutron Script

```
#!/bin/bash

# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING NEUTRON DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create neutron database and grant access to neutron user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS neutron; CREATE DATABASE neutron; GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/neutron_db.pass)'; GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'%' IDENTIFIED BY '$(cat $HOME/secrets/neutron_db.pass)'"

# Neutron user and service setup

# 0 - Source the admin credentials created while installing keystone
source $HOME/open-rcs/admin-openrc.sh

# 1 - Create the neutron user
openstack user create --domain default --password $(cat $HOME/secrets/neutron.pass) neutron

# 2 - Add admin role to neutron on service project
openstack role add --project service --user neutron admin

# 3 - Create neutron/network service entity
openstack service create --name neutron --description "Openstack Networking" network

# 4 - Create service API endpoints
openstack endpoint create --region RegionOne network public http://controller:9696
openstack endpoint create --region RegionOne network internal http://controller:9696
openstack endpoint create --region RegionOne network admin http://controller:9696

# Installing the service package
sudo apt install neutron-server neutron-plugin-ml2 neutron-openvswitch-agent neutron-dhcp-agent neutron-metadata-agent neutron-l3-agent -y

# Removing old/default conf
sudo rm /etc/neutron/neutron.conf

# Copying the correct conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/neutron.conf /etc/neutron/neutron.conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/metadata_agent.ini /etc/neutron/metadata_agent.ini
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/dhcp_agent.ini /etc/neutron/dhcp_agent.ini
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/l3_agent.ini /etc/neutron/l3_agent.ini
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/ml2_conf.ini /etc/neutron/plugins/ml2/ml2_conf.ini
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/openvswitch_agent.ini /etc/neutron/plugins/ml2/openvswitch_agent.ini

# Replacing the db password
sudo sed -i "s/DB_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/neutron_db.pass)/g" /etc/neutron/neutron.conf
# Replacing the nova password
sudo sed -i "s/NOVA_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/nova.pass)/g" /etc/neutron/neutron.conf
# Replacing the rabbitmq password
sudo sed -i "s/RABBIT_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/rabbitmq.pass)/g" /etc/neutron/neutron.conf
# Replacing the neutron user password
sudo sed -i "s/PASSWORD_TO_REPLACE/$(cat $HOME/secrets/neutron.pass)/g" /etc/neutron/neutron.conf
# Replacing the metadata secret
sudo sed -i "s/METADATA_SECRET_TO_REPLACE/$(cat $HOME/secrets/metadata.secret)/g" /etc/neutron/metadata_agent.ini
# Replacing the overlay interface IP Address
sudo sed -i "s/OVERLAY_IP_ADDRESS/$(cat $HOME/conf/overlayip.txt)/g" /etc/neutron/plugins/ml2/openvswitch_agent.ini

# Create provider bridge and link it to IP

# Here we create our bridge, it is an OVS bridge, so it obviously requires OVS to be installed
bridge=br-ex
addbr_cmd="sudo ovs-vsctl --no-wait -- --may-exist add-br $bridge -- set bridge $bridge protocols=OpenFlow13,OpenFlow15"

$addbr_cmd
sudo ovs-vsctl --no-wait br-set-external-id $bridge bridge-id $bridge

# This whole part is there to seamlessly transfer the IP while ensuring no downtime and that your IP won't be lost into the void therefore making
# your server unreachable (yes I speak from experience, it happened to me and I had to completely reinstall it. It was a mess)
from_intf=enp5s0f1
to_intf=br-ex
add_ovs_port=True
del_ovs_port=False
af=inet

IP_REPLACE=""
IP_DEL=""
IP_UP=""
DEFAULT_ROUTE_GW=$(ip -f $af r | awk "/default.+$from_intf\s/ { print \$3; exit }")
ADD_OVS_PORT=""
DEL_OVS_PORT=""
ARP_CMD=""

IP_BRD=$(ip -f $af a s dev $from_intf scope global primary | grep inet | awk '{ print $2, $3, $4; exit }')

if [ "$DEFAULT_ROUTE_GW" != "" ]; then
  ADD_DEFAULT_ROUTE="sudo ip -f $af r replace default via $DEFAULT_ROUTE_GW dev $to_intf"
fi

if [[ "$add_ovs_port" == "True" ]]; then
  ADD_OVS_PORT="sudo ovs-vsctl --may-exist add-port $to_intf $from_intf"
fi

if [[ "$del_ovs_port" == "True" ]]; then
  DEL_OVS_PORT="sudo ovs-vsctl --if-exists del-port $from_intf $to_intf"
fi

if [[ "$IP_BRD" != "" ]]; then
  IP_DEL="sudo ip addr del $IP_BRD dev $from_intf"
  IP_REPLACE="sudo ip addr replace $IP_BRD dev $to_intf"
  IP_UP="sudo ip link set $to_intf up"
  if [[ "$af" == "inet" ]]; then
    IP=$(echo $IP_BRD | awk '{ print $1; exit }' | grep -o -E '(.*)/' | cut -d "/" -f1)
    ARP_CMD="sudo arping -A -c 3 -w 5 -I $to_intf $IP "
  fi
fi

$DEL_OVS_PORT; $IP_DEL; $IP_REPLACE; $IP_UP; $ADD_OVS_PORT; $ADD_DEFAULT_ROUTE; $ARP_CMD

# Populate neutron DB
sudo su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron

# Restart services
sudo service nova-api restart
sudo service neutron-server restart
sudo service neutron-openvswitch-agent restart
sudo service neutron-dhcp-agent restart
sudo service neutron-metadata-agent restart
sudo service neutron-l3-agent restart
```

As you can see, this script is much more complex than even the one in Nova. We have 3 main parts:

#### DB and conf install

```
#!/bin/bash

# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING NEUTRON DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create neutron database and grant access to neutron user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS neutron; CREATE DATABASE neutron; GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/neutron_db.pass)'; GRANT ALL PRIVILEGES ON neutron.* TO 'neutron'@'%' IDENTIFIED BY '$(cat $HOME/secrets/neutron_db.pass)'"

# Neutron user and service setup

# 0 - Source the admin credentials created while installing keystone
source $HOME/open-rcs/admin-openrc.sh

# 1 - Create the neutron user
openstack user create --domain default --password $(cat $HOME/secrets/neutron.pass) neutron

# 2 - Add admin role to neutron on service project
openstack role add --project service --user neutron admin

# 3 - Create neutron/network service entity
openstack service create --name neutron --description "Openstack Networking" network

# 4 - Create service API endpoints
openstack endpoint create --region RegionOne network public http://controller:9696
openstack endpoint create --region RegionOne network internal http://controller:9696
openstack endpoint create --region RegionOne network admin http://controller:9696

# Installing the service package
sudo apt install neutron-server neutron-plugin-ml2 neutron-openvswitch-agent neutron-dhcp-agent neutron-metadata-agent neutron-l3-agent -y

# Removing old/default conf
sudo rm /etc/neutron/neutron.conf

# Copying the correct conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/neutron.conf /etc/neutron/neutron.conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/metadata_agent.ini /etc/neutron/metadata_agent.ini
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/dhcp_agent.ini /etc/neutron/dhcp_agent.ini
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/l3_agent.ini /etc/neutron/l3_agent.ini
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/ml2_conf.ini /etc/neutron/plugins/ml2/ml2_conf.ini
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/neutron/openvswitch_agent.ini /etc/neutron/plugins/ml2/openvswitch_agent.ini

# Replacing the db password
sudo sed -i "s/DB_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/neutron_db.pass)/g" /etc/neutron/neutron.conf
# Replacing the nova password
sudo sed -i "s/NOVA_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/nova.pass)/g" /etc/neutron/neutron.conf
# Replacing the rabbitmq password
sudo sed -i "s/RABBIT_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/rabbitmq.pass)/g" /etc/neutron/neutron.conf
# Replacing the neutron user password
sudo sed -i "s/PASSWORD_TO_REPLACE/$(cat $HOME/secrets/neutron.pass)/g" /etc/neutron/neutron.conf
# Replacing the metadata secret
sudo sed -i "s/METADATA_SECRET_TO_REPLACE/$(cat $HOME/secrets/metadata.secret)/g" /etc/neutron/metadata_agent.ini
# Replacing the overlay interface IP Address
sudo sed -i "s/OVERLAY_IP_ADDRESS/$(cat $HOME/conf/overlayip.txt)/g" /etc/neutron/plugins/ml2/openvswitch_agent.ini
```

This part is mostly the same as the other services, with neutron specificities. The confs are copied and values are replaced with the confs set into the secret and conf files.

#### Bridge creation and IP moving

```
# Create provider bridge and link it to IP

# Here we create our bridge, it is an OVS bridge, so it obviously requires OVS to be installed
bridge=br-ex
addbr_cmd="sudo ovs-vsctl --no-wait -- --may-exist add-br $bridge -- set bridge $bridge protocols=OpenFlow13,OpenFlow15"

$addbr_cmd
sudo ovs-vsctl --no-wait br-set-external-id $bridge bridge-id $bridge

# This whole part is there to seamlessly transfer the IP while ensuring no downtime and that your IP won't be lost into the void therefore making
# your server unreachable (yes I speak from experience, it happened to me and I had to completely reinstall it. It was a mess)
from_intf=enp5s0f1
to_intf=br-ex
add_ovs_port=True
del_ovs_port=False
af=inet

IP_REPLACE=""
IP_DEL=""
IP_UP=""
DEFAULT_ROUTE_GW=$(ip -f $af r | awk "/default.+$from_intf\s/ { print \$3; exit }")
ADD_OVS_PORT=""
DEL_OVS_PORT=""
ARP_CMD=""

IP_BRD=$(ip -f $af a s dev $from_intf scope global primary | grep inet | awk '{ print $2, $3, $4; exit }')

if [ "$DEFAULT_ROUTE_GW" != "" ]; then
  ADD_DEFAULT_ROUTE="sudo ip -f $af r replace default via $DEFAULT_ROUTE_GW dev $to_intf"
fi

if [[ "$add_ovs_port" == "True" ]]; then
  ADD_OVS_PORT="sudo ovs-vsctl --may-exist add-port $to_intf $from_intf"
fi

if [[ "$del_ovs_port" == "True" ]]; then
  DEL_OVS_PORT="sudo ovs-vsctl --if-exists del-port $from_intf $to_intf"
fi

if [[ "$IP_BRD" != "" ]]; then
  IP_DEL="sudo ip addr del $IP_BRD dev $from_intf"
  IP_REPLACE="sudo ip addr replace $IP_BRD dev $to_intf"
  IP_UP="sudo ip link set $to_intf up"
  if [[ "$af" == "inet" ]]; then
    IP=$(echo $IP_BRD | awk '{ print $1; exit }' | grep -o -E '(.*)/' | cut -d "/" -f1)
    ARP_CMD="sudo arping -A -c 3 -w 5 -I $to_intf $IP "
  fi
fi

$DEL_OVS_PORT; $IP_DEL; $IP_REPLACE; $IP_UP; $ADD_OVS_PORT; $ADD_DEFAULT_ROUTE; $ARP_CMD
```

This part is the most specific and complex. It creates an OVS bridge, that is then used by neutron to manage the networking.
This bridge requires the IP used by the interface associated which requires the commands to transfer it to be run fast in
order to avoid downtime on this IP.

#### DB Population and services restart

```
# Populate neutron DB
sudo su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron

# Restart services
sudo service nova-api restart
sudo service neutron-server restart
sudo service neutron-openvswitch-agent restart
sudo service neutron-dhcp-agent restart
sudo service neutron-metadata-agent restart
sudo service neutron-l3-agent restart
```

The last part of the neutron install is to just synchronize the db model and then restart the services so they can take the conf changes into account.

### Nat configuration

```
#!/bin/bash

# Setup iptables conf
# Masquerade outgoing traffic through public interface
sudo iptables -t nat -A POSTROUTING -o enp5s0f0 MASQUERADE
# Accept all coming from ovs bridge
sudo iptables -t nat -A INPUT -i br-ex -j ACCEPT
# Accept all output
sudo iptables -A OUTPUT -j ACCEPT
# Accept all on public interface if a connection is already established
sudo iptables -A INPUT -i enp5s0f0 -m state --state ESTABLISHED,RELATED -j ACCEPT
```

This configuration allows for the VMs to go out and into the internet by allowing forwarding for the br-ex interface.

### Init Neutron Script

```
#!/bin/bash

# Source openstack credentials
source $HOME/open-rcs/admin-openrc.sh

# Create neutron provider network
openstack network create --share --project Default --description "Provider network" --provider-network-type flat --provider-physical-network provider --external --default PublicAccessNetwork

# Create neutron provider subnet
SUBNET_GATEWAY=$(cat $HOME/conf/overlayip.txt)
SUBNET_RANGE=$(cat $HOME/conf/subnet_range.txt)
ALLOCATION_START=$(cat $HOME/conf/allocation_start.txt)
ALLOCATION_END=$(cat $HOME/conf/allocation_end.txt)
openstack subnet create --project Default --network PublicAccessNetwork --description "Public Pool" --gateway $SUBNET_GATEWAY --subnet-range $SUBNET_RANGE --allocation-pool "start=$ALLOCATION_START,end=$ALLOCATION_END" --dns-nameserver 8.8.8.8 PublicPool

# Create Security group
openstack security group create --description "Test Group" test
openstack security group rule create test --protocol tcp --remote-ip 0.0.0.0/0 --dst-port 1:65535
openstack security group rule create test --protocol icmp --remote-ip 0.0.0.0/0
```

This script creates a public network and a public network pool in order to allow VMs to have an interface facing the br-ex interface of the server thus allowing them to be accessible by the server. A security group is also created to open all access to the servers and avoid issues such as ssh being blocked.

### Uninstall Neutron Script

```
#!/bin/bash

# Remove packages
sudo apt remove neutron-server neutron-plugin-ml2 neutron-openvswitch-agent neutron-dhcp-agent neutron-metadata-agent neutron-l3-agent -y

# Remove conf
sudo rm /etc/neutron/neutron.conf

# Source credentials
source $HOME/open-rcs/admin-openrc.sh

# Remove nova user and service
openstack user delete neutron
openstack service delete network

# Cleanup ovs bridges
sudo ovs-vsctl del-br br-ex
```

This script removes the keystone users, the apt installs and the neutron conf as well as deleting the br-ex bridge.