# How to Install Cinder

Cinder is the block storage service of Openstack. It is used for creating volumes and works with Nova and Glance. Through it, you can create multiple volumes using different backends and attach them to running VMs. This is especially useful as it allows for more persistent storage than the base VM.

## Installing on an Ubuntu server

In order to install Cinder on a *single node* you will have to do a mix of the install from the [controller node install guide](https://docs.openstack.org/cinder/2024.1/install/cinder-controller-install-ubuntu.html) and the [storage node install guide](https://docs.openstack.org/cinder/2024.1/install/cinder-storage-install-ubuntu.html).

That requires multiple complex steps so, as usual, let's get through it using scripts to avoid running a lot of commands manually (though you should probably run them at least once just to learn).

### Before the install script

Yes, I know, what's the point of the install script if not installing everything without manually doing anything. But trust me, this one you want to do manually.

First, a small explanation, on the setup that I did, cinder uses as a backend a volume group, so I have to create it. But be careful for a volume group needs to have enough room and most importantly, not break the rest of your install. For this reason, it is most often created on a dedicated disk.

As it turns out, my server did not have one. So I had to make do with creating a partition at the server install on my OVH server, and then unmounting it and removing it from the conf, before finally being able to create the physical volume and the volume group that is used by cinder. Once that is done, you are good to go. Cinder storage node install guide contains the info you need to create the PV and VG.

### Install cinder script

```
#!/bin/bash

# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING CINDER DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create cinder database and grant access to cinder user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS cinder; CREATE DATABASE cinder; GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/cinder_db.pass)'; GRANT ALL PRIVILEGES ON cinder.* TO 'cinder'@'%' IDENTIFIED BY '$(cat $HOME/secrets/cinder_db.pass)'"

# Cinder user and service setup

# 0 - Source the admin credentials created while installing keystone
source $HOME/open-rcs/admin-openrc.sh

# 1 - Create the glance user
openstack user create --domain default --password $(cat $HOME/secrets/cinder.pass) cinder

# 2 - Add admin role to glance on service project
openstack role add --project service --user cinder admin

openstack role add --user cinder --project service service
openstack role add --user nova --project service service

# 3 - Create glance service entity
openstack service create --name cinderv3 --description "Openstack Block Storage" volumev3

# 4 - Create service API endpoints
openstack endpoint create --region RegionOne volumev3 public http://controller:8776/v3/%\(project_id\)s
openstack endpoint create --region RegionOne volumev3 internal http://controller:8776/v3/%\(project_id\)s
openstack endpoint create --region RegionOne volumev3 admin http://controller:8776/v3/%\(project_id\)s

# Installing the service package
sudo apt install -y cinder-api cinder-scheduler lvm2 thin-provisioning-tools cinder-volume tgt cinder-backup

# Removing old/default conf
sudo rm /etc/cinder/cinder.conf
sudo rm /etc/lvm/lvm.conf
sudo rm /etc/tgt/conf.d/cinder.conf

# Copying the correct conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/cinder/cinder.conf /etc/cinder/cinder.conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/cinder/lvm.conf /etc/lvm/lvm.conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/cinder/tgt.conf /etc/tgt/conf.d/cinder.conf
# Replacing the db password
sudo sed -i "s/DB_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/cinder_db.pass)/g" /etc/cinder/cinder.conf
# Replacing the rabbitmq password
sudo sed -i "s/RABBIT_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/rabbitmq.pass)/g" /etc/cinder/cinder.conf
# Replacing the cinder user password
sudo sed -i "s/PASSWORD_TO_REPLACE/$(cat $HOME/secrets/cinder.pass)/g" /etc/cinder/cinder.conf

# Replace management interface IP address
sudo sed -i "s/MANAGEMENT_IP_TO_REPLACE/$(cat $HOME/conf/managementip.txt)/g" /etc/cinder/cinder.conf

# Populate cinder db
sudo su -s /bin/sh -c "cinder-manage db sync" cinder

# Restart nova
sudo service nova-api restart

# Restart cinder services and apache2
sudo service cinder-scheduler restart
sudo service apache2 restart
sudo service tgt restart
sudo service cinder-volume restart
sudo service cinder-backup restart
sudo service iscsid restart

exit 0
```

Here we have the classic keystone user, service and endpoint creation followed by the package install and the conf copy and replace. Here we need to also replace the rabbitmq password and management IP address. We end with DB schema install as well as restarting nova and all cinder services as well as tgt for volume handling and iscsid because it does not work otherwise.