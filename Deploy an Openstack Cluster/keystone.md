# How to Install Keystone

Installing the Keystone identity service is the first step in deploying an Openstack cluster as it is the keystone service that will handle any and all access management. Keystone can either work alone or use external user management services (LDAP). It also manages the endpoints of the services, meaning that it lists the access points for every service.

## Installing on an Ubuntu server

In order to install Keystone on an Ubuntu server, it is recommended to follow the [procedure given on the Openstack website](https://docs.openstack.org/keystone/2024.1/install/get-started-ubuntu.html).

I have personnaly created 2 scripts to help me set this up, as you can see they are quite simple:

### Install keystone:
```
#!/bin/sh

# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING KEYSTONE DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create keystone database and grant access to keystone user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS keystone; CREATE DATABASE keystone; GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/keystone_db.pass)'; GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'%' IDENTIFIED BY '$(cat $HOME/secrets/keystone_db.pass)'"

# Install keystone package and ensure apache2 is installed
sudo apt install apache2
sudo apt install keystone

# Remove old conf
sudo rm /etc/keystone/keystone.conf

# Copy conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/keystone/keystone.conf /etc/keystone/keystone.conf
# Replace in conf to have correct password
sudo sed -i "s/DB_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/keystone_db.pass)/g" /etc/keystone/keystone.conf

# Populate the db
sudo su -s /bin/sh -c "keystone-manage db_sync" keystone

# Initialize Fernet key repositories
sudo keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
sudo keystone-manage credential_setup --keystone-user keystone --keystone-group keystone

# Bootstrap keystone
sudo keystone-manage bootstrap --bootstrap-password $(cat $HOME/secrets/keystone_admin.pass) \
  --bootstrap-admin-url http://controller:5000/v3/ \
  --bootstrap-internal-url http://controller:5000/v3/ \
  --bootstrap-public-url http://controller:5000/v3/ \
  --bootstrap-region-id RegionOne

# Change server name in apache2 conf
sudo rm /etc/apache2/apache2.conf
sudo ln -s $HOME/git_repos/pokkihjus-openstack-infrastructure/config/keystone/apache2.conf /etc/apache2/apache2.conf

# Restart apache2
sudo service apache2 restart

# Generate admin account openrc
echo "export OS_USERNAME=admin" > $HOME/open-rcs/admin-openrc.sh
echo "export OS_PASSWORD=$(cat $HOME/secrets/keystone_admin.pass)" >> $HOME/open-rcs/admin-openrc.sh
echo "export OS_PROJECT_NAME=admin" >> $HOME/open-rcs/admin-openrc.sh
echo "export OS_USER_DOMAIN_NAME=Default" >> $HOME/open-rcs/admin-openrc.sh
echo "export OS_PROJECT_DOMAIN_NAME=Default" >> $HOME/open-rcs/admin-openrc.sh
echo "export OS_AUTH_URL=http://controller:5000/v3" >> $HOME/open-rcs/admin-openrc.sh
echo "export OS_IDENTITY_API_VERSION=3" >> $HOME/open-rcs/admin-openrc.sh
```

This script straight up follows the tutorial, although it uses a few special elements. First of, every password is extracted from the ```~/secrets/``` folder of the user running the script, meaning the password files have to exist. Also, in order to avoid using always the same db pass and having a password in git, the config file contains placeholders instead of the actual passwords, meaning that once copied, these placeholders need to be replaced using the ```sed``` command. At the end of the script, the admin-openrc.sh file is created so as to have a credential file usable for this keystone install.

### Init keystone:
```
#!/bin/bash

# Source credentials
source $HOME/open-rcs/admin-openrc.sh

# Create a domain
# For now this is commented as a default domain is created at bootstrap
# openstack domain create --description "Main Domain" main

# Create service project
openstack project create --domain default --description "Service Project" service

# Create infra project
openstack project create --domain default --description "Infrastructure Project" infra

# Create infra user
openstack user create --domain default --password $(cat $HOME/secrets/infra.pass) infra

# Create infra role
openstack role create infra

# Add Infra role to project and user
openstack role add --project infra --user infra infra

# Create openrc for infra user
echo "export OS_USERNAME=infra" > $HOME/open-rcs/infra-openrc.sh
echo "export OS_PASSWORD=$(cat $HOME/secrets/infra.pass)" >> $HOME/open-rcs/infra-openrc.sh
echo "export OS_PROJECT_NAME=infra" >> $HOME/open-rcs/infra-openrc.sh
echo "export OS_USER_DOMAIN_NAME=Default" >> $HOME/open-rcs/infra-openrc.sh
echo "export OS_PROJECT_DOMAIN_NAME=Default" >> $HOME/open-rcs/infra-openrc.sh
echo "export OS_AUTH_URL=http://controller:5000/v3" >> $HOME/open-rcs/infra-openrc.sh
echo "export OS_IDENTITY_API_VERSION=3" >> $HOME/open-rcs/infra-openrc.sh
```

This script is much shorter than the previous one. Instead of configuration and install, this one initializes the service project, used for every other service install and creates the infra project which is a project I will personnaly use for my needs. It then creates an infra-openrc.sh file, so I can use it afterwards before running my terraform.
