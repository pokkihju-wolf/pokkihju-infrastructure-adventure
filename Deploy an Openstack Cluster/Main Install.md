# How to install an Openstack cluster on ONE and only ONE machine

In order to install an entire openstack cluster on only ONE machine, some tricks will be needed. However, they will be explained in every install guide in this folder.

***NOTE THAT ALL COMMANDS PROBABLY REQUIRE ```sudo``` IF YOUR ARE NOT RUNNING AS ROOT. BE AWARE OF THAT.***

For now, let's get started. Multiple things are needed in order to install an openstack cluster but first and perhaps most important is that you need to have at least 2 servers. Just kidding, though if you can have 2 servers and have one act as control node and the other as a compute node you WILL get much better performance and much more resources on your compute node. You need to count around maybe 20GB of RAM on a node that does everything that will be used solely for controle plane stuff, although it will probably average more around 10-15GB of RAM used.

## Prerequisites

### Step 0: Connectivity and security
The real first thing you need to ensure on your server(s) is that connectivity is established, for that, you can follow the [Openstack Networking Guide](https://docs.openstack.org/install-guide/environment-networking.html). They give an example configuration but what you need is to understand how and why this needs to exist and how it works.  
This tutorial is for installing on a single node so there is not much networking to do, however, we can add to ```/etc/hosts``` the following elements on the ```127.0.1.1``` line:

```
127.0.1.1 yourhostname yourhostname controller compute1
```

What this does is basically force our computer to resolve the ```controller``` dns name and the ```compute1``` dns name to our loopback address ```127.0.1.1```. This allows us to more easily follow the Openstack tutorials without too many changes to the conf. You can try to ping them if you want it should return you something.

Once you have that you also need to prepare the passwords you will use, as are mentionned [here](https://docs.openstack.org/install-guide/environment-security.html). Note that even though it is not mentionned, placement needs a ```DBPASS```. I advise you to either keep them in your mind or password manager but if you don't care about security just store them in files. Once you have that we are ready to go!

### Step 1: NTP

Once that is done, you can configure [NTP](https://fr.wikipedia.org/wiki/Network_Time_Protocol) (if you have multiple servers, if you only have one, this is less important) using the [NTP Install Guide](https://docs.openstack.org/install-guide/environment-ntp.html) of Openstack still. Most steps are documented on the [Openstack Install Guide](https://docs.openstack.org/install-guide/) so if you feel like it you can just jump on there and try to do it yourself.  
I have not done this as it is a single node install, no need to sync time as our own time should always be coherent with ourselves.

### Step 2: Enable Openstack Repository

The next step (also a very important one) is to [enable the openstack package repositories](https://docs.openstack.org/install-guide/environment-packages.html). As I am installing on Ubuntu, I will be following the ubuntu guide to activate the ***2024.1(Caracal)*** repository with the below command:

```
add-apt-repository cloud-archive:caracal
```

Note that this **DOES NOT** install the services and packages, it only *enables* the apt repository to later do so ourselves.  
Once that is done we can just install the openstack client using:

```
apt install python3-openstackclient
```

And *voilà* we have the caracal openstack client.

### Step 3: Install and start the Database

This step, as all others, is very important, but unlike some others, like NTP which will only cause issues once your services are up and running, the database will be a **BLOCKING POINT** if you do not install it or do not install it correctly. So be careful to follow the [tutorial](https://docs.openstack.org/install-guide/environment-sql-database.html) appropriate for your distribution and look for help if you need.  

Here is what I have done, again, this is for a single node cluster, which you will only want/create as either a POC or a personnal homelab, if you run it on prod, you might want a dedicated database server as it can be quite heavy to run it on the controle node, in any case, ONLY ONE DATABASE MUST EXIST, do not install multiple database servers. ***This is not suitable for PRODUCTION environments!!!***  

First step, install mariadb server and pymysql:
```
apt install mariadb-server python3-pymysql
```
This will allow the openstack apps to connect to the db.  

Next, our second step, as in the tutorial linked above, create and/or modify the ```/etc/mysql/mariadb.conf.d/99-openstack.cnf``` and add/change this:

```
[mysqld]
bind-address = 127.0.1.1

default-storage-engine = innodb
innodb_file_per_table = on
max_connections = 4096
collation-server = utf8_general_ci
character-set-server = utf8
```

This will make it so our DB listens on our loopback address that maps to controller on our ```/etc/hosts```.  

The third and final step is to restart the service and run the utility program: ```mysql_secure_installation```. This will let you configure your db and set a root pass (choose it well).

### Step 4: Install and start the RabbitMQ message queue

A message queue is a very useful thing, it allows the services to communicate without issues. As with the database above, this is only installed on one node (not an issue in our case) but if you want to install a multi node cluster, be careful to only have one message queue in order not to break the services that use it to communicate.

Here is the [tutorial](https://docs.openstack.org/install-guide/environment-messaging.html), as usual we will follow the Ubuntu guide.

First, install RabbitMQ:

```
apt install rabbitmq-server
```

Second, create our openstack user:

```
rabbitmqctl add_user openstack RABBIT_PASS
```
(note that here RABBIT_PASS must be replaced using the password you prepared in [Step 0](#step-0-connectivity-and-security))

And third and last command for RabbitMQ setup, we setup the permissions of our user:

```
rabbitmqctl set_permissions openstack ".*" ".*" ".*"
```

### Step 5: Install Memcached for our cache

In this step we will install [```memcached```](https://docs.openstack.org/install-guide/environment-memcached.html). This is used by ```Keystone``` and ```Horizon (Dasboard)``` though in very different ways. The first one uses it to cache the identity tokens and the second for requests. But in both cases it is important to configure it well.

The install is once again, just a few steps:  

First, install the package:

```
apt install memcached python3-memcache
```

Second, configure by adding/changing the following to ```/etc/memcached.conf```:

```
-l 127.0.1.1
```
(Here it is important that you use the correct ip address otherwise it WILL NOT WORK, the services won't be able to connect to memcache)

And third, restart:

```
service memcached restart
```

### Step 6: Etcd, our final prerequisite

What is etcd ? Well, it is a key/value store that will be used in order to store many things from config to service liveness.

The install is as before, easy:

Install package:

```
apt install etcd
```

Configure by creating/modifying the ```/etc/default/etcd``` file:

```
ETCD_NAME="controller"
ETCD_DATA_DIR="/var/lib/etcd"
ETCD_INITIAL_CLUSTER_STATE="new"
ETCD_INITIAL_CLUSTER_TOKEN="etcd-cluster-01"
ETCD_INITIAL_CLUSTER="controller=http://127.0.1.1:2380"
ETCD_INITIAL_ADVERTISE_PEER_URLS="http://127.0.1.1:2380"
ETCD_ADVERTISE_CLIENT_URLS="http://127.0.1.1:2379"
ETCD_LISTEN_PEER_URLS="http://127.0.1.1:2380"
ETCD_LISTEN_CLIENT_URLS="http://127.0.1.1:2379"
```

Finally, restart:

```
systemctl enable etcd
systemctl restart etcd
```

And now you are ready to install the services

## Installing the services

There are a few services you will need as prerequisites for others and also a few "Core" services of Openstack. Here are the services you NEED and the order in which you need them (do not change the order you install them in if you do not know what you are doing).

1. [Keystone](https://docs.openstack.org/keystone/2024.1/install/), you ***NEED*** this one first, if you think you know enough to not install it first, you DO NOT know enough. This is the basis of all the other and handles the identity (users) used by every other service.
2. [Glance](https://docs.openstack.org/glance/2024.1/install/) is the Image service. You will need it in order to have images to start your VMs later. You absolutely need it before Nova.
3. [Placement](https://docs.openstack.org/placement/2024.1/install/) was originally a nova component but it was moved out. It is the service used to orchestrate the VMs.
4. [Nova](https://docs.openstack.org/nova/2024.1/install/) is the computing component of Openstack. It is the core of what will allow for VM creation.
5. [Neutron](https://docs.openstack.org/neutron/2024.1/install/) is the networking component. It is essential to access the VMs and let the communicate. It is also one of the most difficult elements to setup as fucking up your network may break your host which is why it is recommended not to have anything important on said host.

The ones below are less essential:
- [Cinder](https://docs.openstack.org/cinder/2024.1/install/) is the block storage service, essential for data persistence and volume creation for your VMs.
- [Horizon](https://docs.openstack.org/horizon/2024.1/install/) is the dashboard. Very useful but you can do without it as long as you have access to your host or have set up access to your services as you can use the CLI.
- [Swift](https://docs.openstack.org/swift/2024.1/install/) is the Object storage service, very useful but far from essential.
