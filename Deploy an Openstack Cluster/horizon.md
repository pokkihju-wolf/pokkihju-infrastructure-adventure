# How to Install Horizon

Horizon is the dashboard of Openstack.

## Installing on an Ubuntu server

In order to install Nova on a *single node* you will have to do a mix of the install from the [controller node install guide](https://docs.openstack.org/nova/2024.1/install/controller-install-ubuntu.html) and the [compute node install guide](https://docs.openstack.org/nova/2024.1/install/compute-install-ubuntu.html).

That requires multiple complex steps so, as usual, let's get through it using scripts to avoid running a lot of commands manually (though you should probably run them at least once just to learn).

### Install nova script

```
#!/bin/bash

# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING NOVA DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create nova database and grant access to nova user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS nova; CREATE DATABASE nova; GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'; GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'%' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'"
sudo mysql -e "DROP DATABASE IF EXISTS nova_api; CREATE DATABASE nova_api; GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'; GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'%' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'"
sudo mysql -e "DROP DATABASE IF EXISTS nova_cell0; CREATE DATABASE nova_cell0; GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'; GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'%' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'"

# Nova user and service setup

# 0 - Source the admin credentials created while installing keystone
source $HOME/open-rcs/admin-openrc.sh

# 1 - Create the nova user
openstack user create --domain default --password $(cat $HOME/secrets/nova.pass) nova

# 2 - Add admin role to nova on service project
openstack role add --project service --user nova admin

# 3 - Create nova/compute service entity
openstack service create --name nova --description "Openstack Compute" compute

# 4 - Create service API endpoints
openstack endpoint create --region RegionOne compute public http://controller:8774/v2.1
openstack endpoint create --region RegionOne compute internal http://controller:8774/v2.1
openstack endpoint create --region RegionOne compute admin http://controller:8774/v2.1

# Installing the service package (including nova-compute as we are on allinone setting)
sudo apt install nova-api nova-conductor nova-novncproxy nova-scheduler nova-compute -y

# Removing old/default conf
sudo rm /etc/nova/nova.conf

# Copying the correct conf
sudo cp $HOME/git_repos/pokkihjus-openstack-infrastructure/config/nova/nova.conf /etc/nova/nova.conf
# Replacing the db password
sudo sed -i "s/DB_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/nova_db.pass)/g" /etc/nova/nova.conf
# Replacing the rabbitmq password
sudo sed -i "s/RABBIT_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/rabbitmq.pass)/g" /etc/nova/nova.conf
# Replacing the placement password
sudo sed -i "s/PLACEMENT_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/placement.pass)/g" /etc/nova/nova.conf
# Replacing the neutron password
sudo sed -i "s/NEUTRON_PASSWORD_TO_REPLACE/$(cat $HOME/secrets/neutron.pass)/g" /etc/nova/nova.conf
# Replacing the neutron metadata secret
sudo sed -i "s/NEUTRON_METADATA_TO_REPLACE/$(cat $HOME/secrets/metadata.secret)/g" /etc/nova/nova.conf
# Replace management interface IP address
sudo sed -i "s/MANAGEMENT_IP_TO_REPLACE/$(cat $HOME/conf/managementip.txt)/g" /etc/nova/nova.conf
# Replacing the nova user password
sudo sed -i "s/PASSWORD_TO_REPLACE/$(cat $HOME/secrets/nova.pass)/g" /etc/nova/nova.conf

# Populate nova-api DB
sudo su -s /bin/sh -c "nova-manage api_db sync" nova

# Register cell0 DB
sudo su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova

# Create cell1
sudo su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova

# Populate nova DB
sudo su -s /bin/sh -c "nova-manage db sync" nova

# Restart services
sudo service nova-api restart
sudo service nova-scheduler restart
sudo service nova-conductor restart
sudo service nova-novncproxy restart
sudo service nova-compute restart

# Discover compute hosts
sudo su -s /bin/sh -c "nova-manage cell_v2 discover_hosts --verbose" nova

sleep 30
sudo su -s /bin/sh -c "nova-manage cell_v2 discover_hosts --verbose" nova
```

This script looks more complex than the previous ones but it is not. Nova requires 3 DB instead of one, which is the reason why this blocks looks heavier:

```
# WARNING, THIS SCRIPT WILL DELETE ANY EXISTING NOVA DB, USE WISELY, ANY DATA SHOULD BE BACKUPED BEFORE RUNNING
# Create nova database and grant access to nova user after dropping potential already existing database
sudo mysql -e "DROP DATABASE IF EXISTS nova; CREATE DATABASE nova; GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'; GRANT ALL PRIVILEGES ON nova.* TO 'nova'@'%' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'"
sudo mysql -e "DROP DATABASE IF EXISTS nova_api; CREATE DATABASE nova_api; GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'; GRANT ALL PRIVILEGES ON nova_api.* TO 'nova'@'%' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'"
sudo mysql -e "DROP DATABASE IF EXISTS nova_cell0; CREATE DATABASE nova_cell0; GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'localhost' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'; GRANT ALL PRIVILEGES ON nova_cell0.* TO 'nova'@'%' IDENTIFIED BY '$(cat $HOME/secrets/nova_db.pass)'"
```

Afterwards we have the classic keystone user, service and endpoint creation followed by the package install and the conf copy and replace. Here we also have to replace the rabbitmq, placement and neutron passwords as they are used in the conf of Nova. Finally we end the nova install by setting up the api DB and the cells, before restarting all the services and discovering the compute host so that we can deploy VMs.

### Init nova script

```
#!/bin/bash

# Source openstack credentials
source $HOME/open-rcs/admin-openrc.sh

# Create flavors

# Memory focused
openstack flavor create --ram 1024 --disk 20 --vcpus 1 --public --description "Tiny, memory focused" m1.tiny
openstack flavor create --ram 2048 --disk 20 --vcpus 1 --public --description "Small, memory focused" m1.small
openstack flavor create --ram 4096 --disk 20 --vcpus 2 --public --description "Medium, memory focused" m1.medium
openstack flavor create --ram 8192 --disk 20 --vcpus 4 --public --description "Large, memory focused" m1.large
openstack flavor create --ram 16384 --disk 20 --vcpus 8 --public --description "XLarge, memory focused" m1.xlarge

# Memory focused Diskless
openstack flavor create --ram 1024 --vcpus 1 --public --description "Tiny, memory focused, diskless" m2.tiny
openstack flavor create --ram 2048 --vcpus 1 --public --description "Small, memory focused, diskless" m2.small
openstack flavor create --ram 4096 --vcpus 2 --public --description "Medium, memory focused, diskless" m2.medium
openstack flavor create --ram 8192 --vcpus 4 --public --description "Large, memory focused, diskless" m2.large
openstack flavor create --ram 16384 --vcpus 8 --public --description "XLarge, memory focused, diskless" m2.xlarge

# CPU focused
openstack flavor create --ram 512 --disk 20 --vcpus 1 --public --description "Tiny, CPU focused" c1.tiny
openstack flavor create --ram 1024 --disk 20 --vcpus 2 --public --description "Small, CPU focused" c1.small
openstack flavor create --ram 2048 --disk 20 --vcpus 4 --public --description "Medium, CPU focused" c1.medium
openstack flavor create --ram 4096 --disk 20 --vcpus 8 --public --description "Large, CPU focused" c1.large
openstack flavor create --ram 8192 --disk 20 --vcpus 16 --public --description "XLarge, CPU focused" c1.xlarge

# CPU focused Diskless
openstack flavor create --ram 512 --vcpus 1 --public --description "Tiny, CPU focused, diskless" c2.tiny
openstack flavor create --ram 1024 --vcpus 2 --public --description "Small, CPU focused, diskless" c2.small
openstack flavor create --ram 2048 --vcpus 4 --public --description "Medium, CPU focused, diskless" c2.medium
openstack flavor create --ram 4096 --vcpus 8 --public --description "Large, CPU focused, diskless" c2.large
openstack flavor create --ram 8192 --vcpus 16 --public --description "XLarge, CPU focused, diskless" c2.xlarge
```

This script focuses on creating flavors used to deploy VMs afterward, there are 2 types, memory focused or CPU focused and each type has a diskless variant for use with volumes and a standard variant.

### Uninstall nova script

```
#!/bin/bash

# Remove packages
sudo apt remove nova-api nova-conductor nova-novncproxy nova-scheduler nova-compute -y

# Remove conf
sudo rm /etc/nova/nova.conf

# Source credentials
source $HOME/open-rcs/admin-openrc.sh

# Remove nova user and service
openstack user delete nova
openstack service delete compute
```

This script is simple and simply removes conf and packages as well as keystone service and user