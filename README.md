# Pokkihju Infrastructure Adventure

Hello everyone,

Welcome to my personnal project where I document my adventures trying to deploy an application on a personnal cloud hosted on a baremetal server I rent from OVH.

The objectives of this project are:
- Get a server (DONE) (Yes this step is only to have something marked as DONE)
- Manage the server (In Progress) (No this can not ever be set to DONE)
- Get a domain name (DONE)
- ~~Deploy a microstack cluster on the server (In Progress)~~
- Deploy an openstack cluster (RAW) on the server (DONE)
- Monitor the infrastructure (Not Started)
- Create a SpringBoot Application (In Progress)
- Provision and launch a personnal cloud (NextCloud) (Not Started)
- Provision and launch a personnal mail server (Not Started)
- Provision and launch a Kubernetes Cluster in the Microstack cluster using Ansible and Terraform (Not Started)
- Deploy the SpringBoot Application to the Kubernetes Cluster (Not Started)
- Manage the Microstack Cluster using IaC (Ansible and Terraform) (Not Started)

As you can see that's quite a lot to do (or not I don't know, I'm not an expert in these things). Anyway, I will document each of these objectives in their own folders on this repository. I will try to be as precise as possible in these documents and include as much code and information as I can.

Please note that I will only write about things I have actually done in this repository which might be why it will mostly be empty. However I hope that with enough time I will be able to fill it a bit more.

I hope this helps some people and if it helped you, that's great!

Have fun out there and see you in the other parts of this project.

## Contributing
Some people may be allowed to contribute but please note that any contribution will be thoroughly reviewed as I will not tolerate to have false information on this repository. Also note that any contribution you make to this project will forever be free and public.

## Project status
This project is currently ongoing.
